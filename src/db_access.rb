
require 'sqlite3'
require 'fileutils'

# used as POC for database interface
class DbAcccess


    # establish sqlite connection
    def initialize(path)
        # connect db 
        @db = SQLite3::Database.new path
        @db.results_as_hash = true
        @db.type_translation = true
    end

    # clear the database of all data
    def clear
        @db.execute("Delete from file_manifest;")
        @db.execute("Delete from dir_manifest;")
    end

    # get configuration values
    def get_config
        config = {}
        rows = @db.execute('select * from config;')
        rows.each do |row|
            row.each 
            config[row['key'].to_sym] = row['value']
        end
        config
    end



    # for batch sql queries
    def transact(stmts)
        @db.transaction do |t|
            stmts.each do |stmt|
                @db.execute stmt
            end
        end
    end

    # run query on the database
    def run_query(query)
        @db.execute query
    end

    # run query and get the first value returned
    def get_first_value(query)
        @db.get_first_value query
    end

    # insert records into the FILE_MANIFEST table
    def commit_file_records(records)
        count = 0

        return count if records.empty?

        stmt = @db.prepare 'insert into FILE_MANIFEST values( ?, ?, ?, ?, ?, null, null);'

        @db.transaction do |tr|
            records.each do |row|
                begin
                    stmt.execute row[:inode], row[:name] , row[:path], File.mtime(row[:path]).to_s, 1
                    count = count + 1
                rescue SQLite3::ConstraintException => e
                    puts "#{e.message}"
                    next
                end
            end
        end

        return  count
    end

    #insert records into the DIR_MANIFEST table
    def commit_dir_records(records)
        count = 0

        return count if records.empty?

        stmt = @db.prepare "insert into DIR_MANIFEST values( ?, ?, ?, 'Y');"

        @db.transaction do |tr|
            records.each do |row|
                begin
                    new_path = row[:path] + '/'
                    stmt.execute row[:inode], row[:name] , row[:path]
                    count = count + 1
                rescue SQLite3::ConstraintException => e
                    puts "#{e.message}"
                    next
                end
            end

        end

        count
    end

    # remove a path from database
    def remove_dir path
        # remove all files & sub folders
        @db.execute ("delete from DIR_MANIFEST where path LIKE '#{path}%';")
        @db.execute ("delete from FILE_MANIFEST where path LIKE '#{path}%';")
    end


    # is the folder/file with ID already in the database
    def id_present?(id, type)
        if (type == :dir)
            count = @db.get_first_value('select count(*) from DIR_MANIFEST where rowid=? ;', id).to_i
        else 
            count = @db.get_first_value('select count(*) from FILE_MANIFEST where rowid=? ;', id).to_i
        end

        return true if count != 0
        false
    end
    
    # is the folder with path in the database
    def dir_present? path
        count = @db.get_first_value("select count(*) from DIR_MANIFEST where path='#{path}' ;").to_i

        return true if count != 0
        false
    end

    # get list of new files
    def new_file_list
        @db.execute('select * from FILE_MANIFEST where new_name is null;')
    end

    # update the new names of files
    def update_names(file_hash)
        stmt = @db.prepare 'update FILE_MANIFEST set new_name=?, chunks=? where path=?;'
        @db.transaction do |t|
            file_hash.each do |file|
                stmt.execute file[:new_name], file[:chunks], file[:path]
            end
        end

    end

    # get details of file, dir by providing path
    def get_details_from_path(type, query)
        case type
        when :query_file
            result = @db.execute "select path, rowid, version, mdtime from file_manifest where path like '%#{query}%'; "
        when :query_dir
            result = @db.execute "select distinct path, rowid from dir_manifest where path like '%#{query}%'; "
        end
        result
    end

    # get details of file, dir by providing ids
    def get_details_ids(type, ids)
        case type
        when :files
            results = []
            ids.each do |id|
                row = @db.execute "select  new_name, flname, chunks from file_manifest where rowid==#{id}; " do |row|
                    file = {}
                    file[:path] = row['flname']
                    file[:name] = row['new_name']
                    file[:chunks] = row['chunks']
                    results.push file
            end
        end

        when :dirs
            results = {}
            results[:dirs] = []
            results[:files] = []
            ids.each do |id|
                path = @db.get_first_value "select path from dir_manifest where rowid==#{id} ;"
                name = @db.get_first_value "select fdname from dir_manifest where rowid==#{id} ;"
                next if path.nil?
                @db.execute("select path from dir_manifest where path like '#{path}%';") do |row|
                    rel_path = row['path']
                    rel_path = rel_path[rel_path.rindex(name) + name.length + 1 .. -1]
                    results[:dirs].push rel_path
                end

                @db.execute("select path, new_name, chunks, max(version) from file_manifest where path like '#{path}%' group by path;") do |row|
                    rel_path = row['path']
                    rel_path = rel_path[rel_path.rindex(name) + name.length + 1 .. -1]
                    file = {}
                    file[:path] = rel_path
                    file[:name] = row['new_name']
                    file[:chunks] = row['chunks']
                    results[:files].push file
                end
            end
        end
        results
    end

    ## monitor methods

    # get list of dirs
    def get_watch_list
        @db.execute("select path from dir_manifest where watch='Y';")
    end


end






