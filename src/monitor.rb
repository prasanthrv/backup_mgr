require 'thread'
require 'rb-inotify'


class Monitor


        def run
                while true do
                        @event_buf = []

                        # wait for filesystem event
                        if IO.select([@notifier.to_io],[],[],2)

                                # process it and pass message to daemon thread
                                @notifier.process
                                @events_m.lock
                                @events_q.push @event_buf
                                @events_m.unlock
                        else

                                # check for messages from daemon thread
                                @notify_m.lock
                                unless @notify_q.empty?
                                        create_notifier
                                        @notify_q.pop
                                end
                                @notify_m.unlock
                        end
                end

        end

        def initialize params

                # get arguments passed from main thread
                @db = params[:db]
                @ev = []
                @events_q = params[:events]
                @events_m = params[:events_m]
                @notify_q = params[:notify_q]
                @notify_m = params[:notify_m]

                # initial notifier
                create_notifier


        end

        def create_notifier
                # get list of watch dirs
                @watch_list = @db.get_watch_list

                # populate notifier
                @notifier = INotify::Notifier.new
                @watch_list.each do |hash|
                        path = hash['path']
                        #next unless Dir.exist? path

                        # watch for specific filesys events
                        @notifier.watch(path, :create,  :close_write) do |event|
                                # push it to event buffer
                                event.flags.each do |flag|
                                        @event_buf << { flag => event.absolute_name}
                                end
                        end
                end
        end

end
