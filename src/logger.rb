

class Logger

    def initialize(dir)
        @log_dir = dir
        Dir.mkdir @log_dir unless Dir.exist? @log_dir

        @old_name = @log_dir + '/log_old.txt'
        @new_name = @log_dir + '/log.txt'

        @file = create_log
    end

    # swap the old log file, and create a new log file
    def create_log
        file = ''
        if File.exist? @new_name
            FileUtils.copy_file(@new_name, @old_name)
            file = File.open(@new_name, 'w')
        else
            file = File.open(@new_name, 'w')
        end
        file
    end

    # put the line as a new log
    def log(message)
        date = Time.now.strftime("%d/%m/%Y %H:%M")
        @file.puts "[#{date}] : #{message}"
        @file.flush
    end
    
end
