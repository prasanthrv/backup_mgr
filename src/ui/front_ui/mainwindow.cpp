#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    pipe_to_daemon("/home/dare/projects/bkp_mgr/other/to_daemon"),
    pipe_from_daemon("/home/dare/projects/bkp_mgr/other/from_daemon"),
    ui(new Ui::MainWindow),
    daemon_running(false),
    query_run(0)
{
    ui->setupUi(this);
    ui->tblQueryDir->hide ();

    change_daemon_status (detect_daemon ());

    add_queries();

    connect (ui->tabWidget, SIGNAL(currentChanged (int)), this, SLOT(cycle_buttons(int)));
    connect (ui->btnAdd, SIGNAL(clicked()), this, SLOT(add()));
    connect (ui->btnRemove, SIGNAL(clicked()), this, SLOT(remove()));
    connect (ui->btnRestore, SIGNAL(clicked()), this, SLOT(restore()));
    connect (ui->txtQuery, SIGNAL(returnPressed()), this, SLOT(query()));
    connect (ui->btnExecute, SIGNAL(clicked()), this, SLOT(execute_actions()));

    emit ui->tabWidget->currentChanged (0);
}

void MainWindow::cycle_buttons(int tab_index)
{
        ui->btnRemove->setEnabled (true);
        ui->btnRestore->setEnabled (true);
        ui->btnAdd->setEnabled (true);
        //ui->btnDaemon->setEnabled (true);
        ui->btnExecute->setEnabled (true);

        switch (tab_index) {
          case 0:
                ui->btnRestore->setEnabled (false);
                ui->btnRemove->setEnabled (false);
            break;
          case 1:
                ui->btnExecute->setEnabled (false);
                ui->btnAdd->setEnabled (false);
           break;
          case 2:
                ui->btnAdd->setEnabled (false);
                ui->btnRemove->setEnabled (false);
                ui->btnRestore->setEnabled (false);
                ui->btnExecute->setEnabled (false);
          default:
            break;
          }
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* INTERFACE RELATED */
// populate hash with query formats
void MainWindow::add_queries ()
{
        operation_strings[Operation::ADD] =  "[{\"op\":\"add\",\"path\":\"%%%Q\"}]\n";
        operation_strings[Operation::QUERY_FILE] =  "[{\"op\":\"query_file\",\"path\":\"%%%Q\"}]\n";
        operation_strings[Operation::QUERY_DIR] =  "[{\"op\":\"query_dir\",\"path\":\"%%%Q\"}]\n";
        operation_strings[Operation::REMOVE_DIR] = "[{\"op\":\"remove_dirs\",\"ids\":[\"%%%Q\"]}]\n" ;
        operation_strings[Operation::REMOVE_FILE] =  "[{\"op\":\"remove_files\",\"ids\":[\"%%%Q\"]}]\n";
        operation_strings[Operation::RESTORE_FILE] = "[{\"op\":\"restore_files\",\"ids\":[\"%%%Q\"],\"path\":\"%%%Y\"}]\n" ;
        operation_strings[Operation::RESTORE_DIR] = "[{\"op\":\"restore_dirs\",\"ids\":[\"%%%Q\"],\"path\":\"%%%Y\"}]\n" ;
}

// format the query for the operation
QString MainWindow::format_query (Operation op, QString query_str, int num_reps = 1)
{
        QString query = operation_strings[op];
        if (num_reps == 1) {
                query.replace("%%%Q", query_str);
        }
        else if (num_reps == 2) {
               QStringList reps = query_str.split (';');
               query.replace ("%%%Q", reps[0]);
               query.replace ("%%%Y", reps[1]);
        }
        return query;
}

// send input to pipe and return output
QString MainWindow::run_action (QString query)
{
        QString old_label = ui->lblDaemon->text ();
        ui->lblDaemon->setText ("Working...");

        QString output = "Query #" + QString::number(++query_run);
        QFile pipe_to (pipe_to_daemon);
        std::string path = pipe_from_daemon.toStdString ();
        FILE *pipe_from;
        pipe_from = fopen(path.c_str(),"r");

        qDebug() << pipe_to_daemon << " " << pipe_from_daemon;

        if (! pipe_to.open(QIODevice::WriteOnly))
                show_error("Cannot open pipe to write. ");

        if (pipe_from == NULL)
                show_error("Cannot open pipe to read. ");

        QTextStream out (&pipe_to);
        out << query;
        pipe_to.flush ();
        pipe_to.close();



        // get output
        char ch;
        while ( (ch = getc (pipe_from)) != EOF) {
                output.append (ch);
                if (output.contains ("END_TRANSMIT") || output.isEmpty ())
                        break;
        }

        // remove END_TRANSMIT
        output.remove (output.lastIndexOf ('\n') + 1, output.length ());

        ui->txtOutput->append (output);

        fclose(pipe_from);

        ui->lblDaemon->setText (old_label);
        return output;
}

// parse query output
bool MainWindow::parse_query_result(Operation op, QString query_result,
                               QList<QHash<QString, QString>> &result)
{
//        Id:  236 Version:   1 Time: 2014-04-15 15:12:46  Path: /home/dare/pictures/troll_spray.jpg
        bool error = false;
        QRegExp rx;

        // remove Query header
        rx.setPattern ("Query.*:.*\\d+\n");
        query_result.remove(rx);

        switch (op) {
                case Operation::QUERY_DIR:
                {
                        QStringList list = query_result.split ('\n');
                        for (auto str = list.constBegin (); str != list.constEnd (); str++) {
                                qDebug() << (*str) << "\n";

                                if ((*str).isEmpty())
                                        continue;
                                QHash<QString, QString> query_data;

                                // Id field
                                rx.setPattern ("\\s*Id:\\s+(\\d+)\\s+");
                                if (rx.indexIn (*str) == -1) {
                                    show_error ("Cannot parse query output : Id");
                                    error = true;
                                    break;
                                  }
                                query_data["id"] = rx.cap(1);

                                // Dir field
                                rx.setPattern ("Dir :\\s+(.*)\\s");
                                if (rx.indexIn (*str) == -1) {
                                    show_error ("Cannot parse query output : Dir");
                                    error = true;
                                    break;
                                  }
                                query_data["dir"] = rx.cap(1);

                                result.append (query_data);
                        }
                }
                break;

                case Operation::QUERY_FILE:
                {
                        QStringList list = query_result.split ('\n');
                        for (auto str = list.constBegin (); str != list.constEnd (); str++) {
                                qDebug() << (*str) << "\n";

                                if ((*str).isEmpty())
                                        continue;
                                QHash<QString, QString> query_data;

                                // Id field
                                rx.setPattern ("\\s*Id:\\s+(\\d+)\\s+");
                                if (rx.indexIn (*str) == -1) {
                                    show_error ("Cannot parse query output : Id");
                                    error = true;
                                    break;
                                  }
                                query_data["id"] = rx.cap(1);

                                // Version field
                                rx.setPattern ("Version:\\s+(\\d+)\\s+");
                                if (rx.indexIn (*str) == -1) {
                                    show_error ("Cannot parse query output : Version");
                                    error = true;
                                    break;
                                  }
                                query_data["ver"] = rx.cap(1);

                                // Date & time
                                rx.setPattern ("Time:\\s+(\\d+-\\d+-\\d+)\\s(\\d+:\\d+:\\d+)\\s+");
                                if (rx.indexIn (*str) == -1) {
                                    show_error ("Cannot parse query output : Date & Time");
                                    error = true;
                                    break;
                                  }
                                query_data["date"] = rx.cap(1);
                                query_data["time"] = rx.cap(2);


                                // Path field
                                rx.setPattern ("Path:\\s+(.*)\\s");
                                if (rx.indexIn (*str) == -1) {
                                    show_error ("Cannot parse query output : Path");
                                    error = true;
                                    break;
                                  }
                                query_data["path"] = rx.cap(1);

                                result.append (query_data);

                        }
                }
                break;

                default:
                        ;
        };
        return error;
}

/* MISC */
// change label to show if daemon is running or not
void MainWindow::change_daemon_status(bool running)
{
        QString status;

        if (running)
                status = "Yes";
        else
                status = "No";

        ui->lblDaemon->setText (status);


}

// detect if daemon is running
// right now, linux specific
bool MainWindow::detect_daemon()
{
        daemon_running = false;

        QProcess proc;
        QString pgm = "/bin/sh -c \"ps ax | grep daemon.[rb]\"";
        proc.start(pgm);
        proc.waitForReadyRead ();

        QString out = proc.readAllStandardOutput ();
        //QMessageBox::about(this, "about", out);
        if (!out.isEmpty ())
                daemon_running = true;

        proc.close ();

        return daemon_running;

}

/* EVENTS */
// add operation
void MainWindow::add()
{
        // Create a file dialog
        QFileDialog dialog(this);
        dialog.setFileMode (QFileDialog::Directory);
        dialog.setOption (QFileDialog::ShowDirsOnly, true);
        dialog.setViewMode (QFileDialog::List);
        dialog.setDirectory ("/home/dare");
        dialog.setMaximumSize (QSize(500,600));

        //Get selected directory
        if (dialog.exec())  {
                QStringList dirs = dialog.selectedFiles ();
                QString dir = dirs[0];

                // create and add action to table
                QStringList contents;
                contents.append("Add");
                contents.append(dir);
                contents.append("Pending");

                add_table_item (ui->tblAction, &contents, 3);

                QMessageBox::about (this, "selected text", dir);
        }



}

// remove path
void MainWindow::remove ()
{
        Operation op;
        QTableWidgetItem *cur;
        int cur_row;


        if (ui->tblQueryDir->isHidden ())
                op = Operation::REMOVE_FILE;
        else
                op = Operation::REMOVE_DIR;


        switch (op) {
                case Operation::REMOVE_FILE:
                {
                        cur = ui->tblQueryFile->currentItem ();
                        if (cur == NULL) { return; }
                        cur_row = cur->row ();
                        QString id = ui->tblQueryFile->item (cur_row, 0)->text ();

                        QStringList list;
                        list.append ("Remove File");
                        list.append (id);
                        list.append ("Pending");
                        add_table_item (ui->tblAction, &list, 3);


                }
                break;
                case Operation::REMOVE_DIR:
                {

                        cur = ui->tblQueryDir->currentItem ();
                        if (cur == NULL) { return; }
                        cur_row = cur->row ();
                        QString id = ui->tblQueryDir->item (cur_row, 0)->text ();

                        QStringList list;
                        list.append ("Remove Dir");
                        list.append (id);
                        list.append ("Pending");
                        add_table_item (ui->tblAction, &list, 3);
                }
                break;
                default :
                        ;
        }


}

// restore path
void MainWindow::restore ()
{
  Operation op;
  QTableWidgetItem *cur;
  QString path;
  int cur_row;

  // Create a file dialog
  QFileDialog dialog(this);
  dialog.setFileMode (QFileDialog::Directory);
  dialog.setOption (QFileDialog::ShowDirsOnly, true);
  dialog.setViewMode (QFileDialog::List);
  dialog.setDirectory ("/home/dare");
  dialog.setMaximumSize (QSize(500,600));

  //Get selected directory
  if (dialog.exec())  {
          QStringList dirs = dialog.selectedFiles ();
          path = dirs[0];
  }

  if (ui->tblQueryDir->isHidden ())
          op = Operation::RESTORE_FILE;
  else
          op = Operation::RESTORE_DIR;


  switch (op) {
          case Operation::RESTORE_FILE:
          {
                  cur = ui->tblQueryFile->currentItem ();
                  if (cur == NULL) { return; }
                  cur_row = cur->row ();
                  QString id = ui->tblQueryFile->item (cur_row, 0)->text ();

                  QStringList list;
                  list.append ("Restore File");
                  list.append (id + " to " + path);
                  list.append ("Pending");
                  add_table_item (ui->tblAction, &list, 3);


          }
          break;
          case Operation::RESTORE_DIR:
          {

                  cur = ui->tblQueryDir->currentItem ();
                  if (cur == NULL) { return; }
                  cur_row = cur->row ();
                  QString id = ui->tblQueryDir->item (cur_row, 0)->text ();

                  QStringList list;
                  list.append ("Restore Dir");
                  list.append (id + " to " + path);
                  list.append ("Pending");
                  add_table_item (ui->tblAction, &list, 3);
          }
          break;
          default :
                  ;
  }
}

// when enter is pressed in the query field
void MainWindow::query()
{
        if (daemon_running) {

                Operation op;
                QString path = ui->txtQuery->text ();

                if (ui->rdDir->isChecked ())
                        op = Operation::QUERY_DIR;
                else
                        op = Operation::QUERY_FILE;

                QString query = format_query (op, path);
                //QMessageBox::about (this, "json text", query);

                QString query_out = run_action (query);

                if (query_out.contains ("not found")) {
                        ui->tblQueryDir->setRowCount (0);
                        ui->tblQueryFile->setRowCount (0);
                        return;
                }

                QList<QHash<QString, QString>> result;
                parse_query_result (op, query_out, result);


                //ui->tblQuery->clearContents ();
                //ui->tblQuery->clear ();

                if (op == Operation::QUERY_FILE) {
                        ui->tblQueryDir->hide ();
                        ui->tblQueryFile->show ();
                        ui->tblQueryFile->setRowCount (0);
                        for (auto iter = result.constBegin (); iter != result.constEnd (); iter++) {
                                        auto item = *iter;
                                        QStringList list;
                                        list.append (item["id"]);
                                        list.append (item["ver"]);
                                        list.append (item["path"]);
                                        list.append (item["date"]);
                                        list.append (item["time"]);
                                        add_table_item (ui->tblQueryFile, &list, 5);
                        }

                }
                else {
                         ui->tblQueryFile->hide ();
                         ui->tblQueryDir->show ();
                         ui->tblQueryDir->setRowCount (0);
                         for (auto iter = result.constBegin (); iter != result.constEnd (); iter++) {
                                        auto item = *iter;
                                        QStringList list;
                                        list.append (item["id"]);
                                        list.append (item["dir"]);
                                        add_table_item (ui->tblQueryDir, &list, 2);
                        }

                }
                                        //show_error(*it);
                //show_error (data["id"]);


        }
        else
                show_error ("Daemon is not running. \n Please start daemon.");
}

// execute pending actions
void MainWindow::execute_actions ()
{

        QList<QTableWidgetItem*> item_list = ui->tblAction->findItems ("Pending", Qt::MatchExactly);

        for (auto item = item_list.constBegin (); item != item_list.constEnd (); item++) {
                int row = (*item)->row ();

                QString op = ui->tblAction->item (row, 0)->text ();
                QString data = ui->tblAction->item (row, 1)->text ();
                QString query;
                QString output;
                QString status = "Failed.";

                if (op == "Add") {
                        query = format_query (Operation::ADD, data);
                }
                else if (op == "Remove File") {
                        query = format_query (Operation::REMOVE_FILE, data);
                }
                else if (op == "Remove Dir") {
                        query = format_query (Operation::REMOVE_DIR, data);
                }
                else if (op == "Restore File") {
                        QStringList str_list = data.split (" ");
                        data = str_list[0] + ";" + str_list[2];
                        query = format_query (Operation::RESTORE_FILE, data, 2);
                }
                else if (op == "Restore Dir") {
                        QStringList str_list = data.split (" ");
                        data = str_list[0] + ";" + str_list[2];
                        query = format_query (Operation::RESTORE_DIR, data, 2);
                }

                output = run_action (query);
                if (output.contains ("Operation completed")) {
                    status = "Success";
                }
                else {
                    status = status + " Reason: " + output.split (' ').last ();
                }

                ui->tblAction->item (row, 2)->setText (status);


        }
}

/* GUI */
// Add action to table
void MainWindow::add_table_item (QTableWidget *table, QStringList *list, int no_cols)
{
        // add row
        int row = table->rowCount () ;
        table->setRowCount (row + 1);

        // iterate and add strings
        int col = 0;
        for(auto iter = list->constBegin (); iter != list->constEnd () || col != no_cols; iter++) {

                QTableWidgetItem *item = new QTableWidgetItem(*iter);
                table->setItem (row, col++, item);
        }
}


// show a message box with the error
void MainWindow::show_error(QString str)
{
        QMessageBox::warning (this,"Error", str);
}
