#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QFileDialog>
#include <QSize>
#include <QStringList>
#include <QMessageBox>
#include <QTableWidget>
#include <QDebug>
#include <QProcess>
#include <QRegExp>

#include <cstdio>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void cycle_buttons(int);
    // remove the dir/ file
    void remove();
    void restore();
    void add();
    void query();
    void execute_actions();

private:
    Ui::MainWindow *ui;
    enum  Operation { ADD, RESTORE_DIR, RESTORE_FILE,
                      QUERY_FILE, QUERY_DIR, REMOVE_FILE,
                      REMOVE_DIR };
    enum response_type { ACTION, ID, PATH, MESSAGE,
                         TIME };


    const QString pipe_to_daemon;
    const QString pipe_from_daemon;

    bool daemon_running;
    int query_run;

    QHash<Operation, QString> operation_strings;


    // Methods

    // detect if daemon is running
    // right now, linux specific
    bool detect_daemon();
    void change_daemon_status(bool running);

    // format the query for the operation
    QString format_query(Operation, QString query_str, int no_reps);
    // populate hash with query formats
    void add_queries();

    // when enter is pressed in the query field
    void add_table_item (QTableWidget *table, QStringList *list, int no_cols);

    // show a message box with the error
    void show_error(QString str);



    // send input to pipe and return output
    QString run_action (QString query);


    // parse query output
    bool parse_query_result(Operation op, QString query_result,
                QList<QHash<QString, QString>> &result);


};

#endif // MAINWINDOW_H
