
require_relative 'utils'
require 'json'


# check if daemon is running, if not run it
if `ps ax | grep bkp_daemo[n]` == ''
end

# parse arguments
options = Util.parse(ARGV)
if options.empty?
        puts 'No arguments found. Use -h for help'
        exit
end

Util.create_pipes

pipe_to_daemon = File.open(Util::PIPES[:to_daemon], 'wb')
pipe_to_daemon.puts(options.to_json)
#Marshal::dump(options, pipe_to_daemon)
pipe_to_daemon.flush
#pipe_to_daemon.close

pipe_from_daemon = File.open(Util::PIPES[:from_daemon], 'r')
response =''
while true do
        response = pipe_from_daemon.gets
        break if response.nil?
        if response.include?('END_TRANSMIT')
                break
        else
                puts response
        end
end
pipe_from_daemon.close

