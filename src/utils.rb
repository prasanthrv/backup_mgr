
require 'securerandom'
require 'mkfifo'
require 'optparse'
require 'find'

# Module for common functions
module Util

    # pipes for communicating with the daemon
    PIPES = { :to_daemon => Dir.pwd + '/other/to_daemon', 
              :from_daemon => Dir.pwd + '/other/from_daemon' 
    }.freeze
    BKP_DIR = Dir.pwd + '/other/bkp/'


    # Get the name of the log file
    def self.get_log_name
        dir_name = Dir.pwd + '/log/'
        file_name = 'log.txt'
        path = dir_name + file_name
        rnd = Random.new

        # Try to make new name as long as the old one exists
        while File.exist? path do
            file_name = 'log_' + rnd.rand(MAX_LOG).to_s + '.txt'
            path = dir_name + file_name
        end
    end

    # Retreive the directories and file names in a directory recursively
    def self.get_dirs_files (dir_path, dirs = [], files =[])
        Find.find(dir_path) { |e|
            if File.directory? e 
                dirs.push e unless '..'.include? e # ignore '.' and '..'
            else    
                files.push e
            end     
        }
    end

    #get inodes for paths in array
    def self.get_records (paths)
        records = []
        return records if paths.empty?
        paths.each do |path|
            record = { }
            record[:inode] = File.stat(path).ino
            record[:name] = File.basename(path)
            record[:path] = path
            records.push record
        end 
        records
    end     

    # parse command line options
    def self.parse(args)
        options = []
        parser = OptionParser.new do |opts|
            opts.banner = 'Usage bkpMgr [command] [arguments]'
            opts.on('-a PATH', '--add PATH', 'Add a path') do |path|
                op = {}
                op[:op] = :add
                op[:path] = path
                options.push op
            end
            opts.on('', '--clear', 'Clear all contents of backup') do |path|
                op = {}
                op[:op] = :clear
                options.push op
            end
opts.on('-d ids', '--remove-files ids', Array, 'Remove files') do |list|
                op = {}
                op[:op] = :remove_files
                op[:ids] = list
                options.push op
            end
            opts.on('-D ids', '--remove-dirs ids', Array, 'Remove directories') do |list|
                op = {}
                op[:op] = :remove_dirs
                op[:ids] = list
                options.push op
            end

            opts.on('-q FILE', '--query-file FILE', 'Query for a filename') do  |query|
                op = {}
                op[:op] = :query_file
                op[:path] = query
                options.push op
            end
            opts.on('-Q DIR', '--query-dir DIR', 'Query for a directory') do  |query|
                op = {}
                op[:op] = :query_dir
                op[:path] = query
                options.push op
            end
            opts.on('-r ids path', '--restore-files ids path', Array, 'Restore a file') do  |list|
                op = {}
                op[:op] = :restore_files
                op[:ids] = list[0..-2]
                op[:path] = list[-1]
                options.push op
            end
            opts.on('-R ids path', '--restore-dirs ids path', Array, 'Restore a directory') do  |list|
                op = {}
                op[:op] = :restore_dirs
                op[:ids] = list[0..-2]
                op[:path] = list[-1]
                options.push op
            end
        end
        parser.parse! args rescue options
        options
    end


    # create named pipes 
    def self.create_pipes 
        PIPES.each do  |pipe, path|
            File.mkfifo path unless File.exist? path
        end
    end

    # get random name for file, one that does not exist
    def self.rnd_name
        flname = SecureRandom.hex(20)
        while File.exist? BKP_DIR + '/' + flname
            flname = SecureRandom.hex(20)
        end
        flname
    end

    # put text to pipe and flush
    def self.puts_pipe(pipe, str)
        pipe.puts str
        pipe.flush
    end

    # close pipe transmission
    def self.end_transmit pipe
        pipe.puts 'END_TRANSMIT'
        pipe.flush
    end

    #get relative path
    def self.get_rel_path(path, name)
        rel_path = path
        rel_path = rel_path[rel_path.rindex(name) + name.length + 1 .. -1]
    end

    # compress or decompress and copy a file
    def self.press_file(src, dest, operation, type, chunks = [])

        # compress, decompress
        if operation == :compress
            case type
            when "snappy"
                chunks = snap_deflate(src, dest)
            end
        else 
            case type 
            when "snappy"
                chunks = snap_inflate(src, dest, chunks)
            end
        end
        chunks
    end

    # use Snappy to uncompress file according to chunk size in database
    def self.snap_deflate(src, dest)
        chunk_list = ''

        File.open(src, "r") { |fdr|
            File.open(dest, "w") { |fdw|
                while buf = fdr.read(1024 * 1024)
                    cmp_buf = Snappy.deflate buf
                    # separator
                    chunk_list += cmp_buf.length.to_s + ';'
                    fdw.write cmp_buf
                end
            }
        }

        chunk_list
    end

    # use Snappy to compress file, and return size of each compressed chunk 
    def self.snap_inflate(src, dest, chunks)

        raise Exception("No chunk sizes given") if chunks.nil?

        File.open(src, "r") do |fdr|
            File.open(dest, "w") do |fdw|
                chunks.each do |chunk| 
                    buf = fdr.read chunk  
                    data = Snappy.inflate buf
                    fdw.write data
                end
            end
        end

    end

end

