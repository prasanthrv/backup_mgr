
require_relative 'utils'
require_relative 'db_access'
require_relative 'monitor'
require_relative 'logger'
require 'snappy'
require 'zlib'
require 'json'
require 'daemons'


PathError = Class.new(Exception)
FileError = Class.new(Exception)


class Daemon

    # constructor
    def initialize

        # open log file
        @log = Logger.new('other/log')

        # intialize database access obj
        @db = DbAcccess.new(Dir.pwd + '/db/index.sqlite')

        # get config data
        @config = @db.get_config

        # initlaize thread objects
        @events_q = Queue.new
        @events_m = Mutex.new
        @notify_q = Queue.new
        @notify_m = Mutex.new
        monitor_params = { :db => @db,
                           :events => @events_q,
                           :events_m => @events_m,
                           :notify_q => @notify_q,
                           :notify_m => @notify_m
                         }


        # create and initialize named pipe
        Util.create_pipes

        # initlaize pipes
        @source_pipe = File.open( Util::PIPES[:to_daemon] , 'rb+')
        @dest_pipe = File.open( Util::PIPES[:from_daemon] , 'w+')

        # create monitor thread
        log 'Creating thread'
        Thread.new {
            m = Monitor.new monitor_params
            m.run
        }

        log 'Finish Init'
    end

    # log message
    def log message
        @log.log message
    end

    def run
        # main loop

        while true do
            # check for messages from front end processes
            if IO.select([@source_pipe], [], [], 3)

                # get json data & parse
                actions = []
                input = @source_pipe.gets
                json_arr = JSON.parse(input)

                # symbolize
                json_arr.each do |hash|
                    actions.push  hash.inject ({}){ |h,(k,v)| h[k.to_sym] = k=='op' ? v.to_sym : v ; h}
                end
                parse_actions actions
            else
                #check events from filesystem monitor
                events = []
                @events_m.lock
                until @events_q.empty?
                    events << @events_q.pop
                end

                events = events.flatten

                @events_m.unlock
                parse_events events unless events.empty?
            end
        end

    rescue Exception => e
        log e.message
        puts e.message
        exit
    end

    # take action of events which are returned by the
    # monitor thread
    def parse_events(events)
        events.each do  |event|
            event.each do |type,path|
                case type
                                
                # a file/directory is created
                when :create

                    log 'receive create event from monitor'
                    # is it a directory?
                    next unless events.detect { |e| e[:isdir] == path }

                    record = {}
                    record[:inode] = File.stat(path).ino
                    record[:name] = File.basename(path)
                    record[:path] = path
                    @db.commit_dir_records [record]

                    notify_monitor

                    log 'Finished processing create event'
                # a file is closed after writing
                when :close_write

                    log 'receive close write event from monitor'
                    # is the file already in the db?
                    query = %{ select * 
                               from file_manifest
                               where path='#{path}';
                             }
                    count = @db.get_first_value query 

                    # if so, 
                    if count != 0
                        # compare file modification times
                        # between database and current
                        mdtime = File.mtime(path)
                        query = %{ select max(mdtime)    
                               from file_manifest   
                               where path='#{path}';
                        }
                        max_mdtime = @db.get_first_value query
                        # if same version, return
                        return if max_mdtime == mdtime.to_s
                    end

                    # get recent version, name etc
                    query = %{ select max(version)  
                               from file_manifest  
                               where path='#{path}';
                    }
                    max_version = @db.get_first_value query
                    max_version = 0 if max_version.nil?
                    new_name = Util.rnd_name


                    # copy file
                    dest = Util::BKP_DIR + new_name
                    chunks = Util.press_file(path, dest, :compress, @config[:compress_type])
                    record = Util.get_records([path])[0]

                    # add file to database with updated values
                    query = %{ insert into file_manifest  
                               values( #{record[:inode]}, '#{record[:name]}',
                                      '#{record[:path]}','#{mdtime}',
                                       #{max_version + 1}, '#{new_name}', 
                                      '#{chunks}');
                            }
                    @db.run_query query

                    log 'Finished processing close write event'
                end
            end
        end
    end

    # take actions on the query run by the front end
    def parse_actions(options)

        action_count = 1
        # process arguments
        options.each do |opt|
            begin
                transmit '    '
                transmit "Action: #{action_count}"
                action_count+=1
                case opt[:op]

                # clear the whole backup
                when :clear

                    # clear db then clear backup directory
                    log 'Start processing clear action'
                    @db.clear
                    path = Util::BKP_DIR
                    system "rm -rf #{path}"
                    Dir.mkdir path.chomp('/')
                    log 'Finished processing clear action'
                    op_complete

                # add the path to the database
                when :add
                    log 'Start processing add action'
                    add_dir(trim opt[:path])


                # remove path from database
                when :remove_files, :remove_dirs 

                    log 'Start processing remove action'
                    # init, trim
                    op = opt[:op]
                    ids =  opt[:ids]


                    # select ids that are integer
                    new_ids = ids.select { |id| id == id.to_i.to_s }

                    # some sanity checking for format of arguments
                    if ids.empty? || new_ids.length != ids.length
                        transmit 'Argument format error'
                        log 'Argument format error'
                        next
                    end
                    ids = new_ids

                    remove(ids, op)

                # query a file name from database
                when :query_file, :query_dir

                    log "Start processing query action"
                    # init, trim
                    type = opt[:op]
                    path = trim opt[:path]

                    query(path,type)

                # restore files, dirs to the destination path
                when :restore_files, :restore_dirs

                    log 'Start processing restore action'
                    # init, trim
                    ids =  opt[:ids]
                    path = trim opt[:path]


                    # select ids that are integer
                    new_ids = ids.select { |id| id == id.to_i.to_s }

                    # some sanity checking for format of arguments
                    if ids.empty? || new_ids.length != ids.length || (path.to_i.to_s == path)
                        transmit 'Argument format error'
                        next
                    end
                    ids = new_ids

                    restore(ids, path, opt[:op])
                
                end
                # if path does not exist
            rescue PathError, FileError => e
                transmit "Path  does not exist"
                log 'Path does not exist'
            end

        end

        Util.end_transmit @dest_pipe
    end

    def trim path
        path.chomp('/')
    end

    def add_dir path

        # does path exist?
        assert_path path
        dirs = []
        files = []


        # Has the dir already been added?
        if @db.dir_present? path
            transmit "#{path} already in database"
            log "#{path} already in database"
            return
        end

        # recursively get files & dirs
        Util.get_dirs_files(path, dirs, files)

        # convert into records
        dir_records = Util.get_records dirs
        file_records = Util.get_records files

        # commit
        transmit "File records: #{ file_records.count }"
        transmit "Committed records: #{ @db.commit_file_records file_records }"
        transmit "Dir records: #{ dir_records.count }"
        transmit "Committed records: #{ @db.commit_dir_records dir_records }"

        # copy new files
        copy_new_files

        # notify monitor
        log "Finished processing add action on #{path}"
        op_complete
        notify_monitor

    end


    def remove(ids, op)


        # is the dir in the database
        ids.each do |id|

            path = ""
            # get path related with ID
            if (op == :remove_dirs)
                path = @db.get_first_value " select path" \
                    " from dir_manifest" \
                    " where rowid=#{id};";
            else 
                path = @db.get_first_value " select path" \
                    " from file_manifest" \
                    " where rowid=#{id};";
            end

            # if ID is not in database
            if ((op == :remove_dirs && !@db.id_present?(id, :dir)) ||
                (op == :remove_files && !@db.id_present?(id, :file)))
                transmit "ID #{id} not present in database"
                log "ID #{id} not present in database"
                next
            end

            # remove files from backup
            file_paths = []
            # get backup paths
            records = @db.run_query " select new_name"    \
                " from file_manifest" \
                " where path like '#{path}%';"
            records.each do |record|
                flpath = Util::BKP_DIR  + record['new_name']
                file_paths.push flpath
            end

            # delete those files
            transmit 'Deleting backup files'
            file_paths.each do |file|
                if File.exist? file
                    File.delete file
                end
            end
            transmit 'Files deleted'


            #remove from db
            if (op == :remove_dirs)
                @db.run_query ("delete from DIR_MANIFEST where path LIKE '#{path}%';")
                @db.run_query ("delete from FILE_MANIFEST where path LIKE '#{path}%';")
            else
                query =  "delete from FILE_MANIFEST where path='#{path}';"
                @db.run_query query
            end
            transmit "Removed path #{path}"

            log "Finished processing remove actions on ids #{p ids}"
            op_complete
        end
        
        notify_monitor
    end
        


    # restore the files or dir pointed by the IDs
    def restore(ids, path, type)

        # does restore path exist?
        assert_path path

        # restore file
        if type == :restore_files

            # ok, restore those files
            files =  @db.get_details_ids(:files, ids).flatten

            if files.empty?
                transmit 'No such ids'
                return
            end

            transmit "Copying #{files.length} files to #{path}"
            restore_files(files,path)

            transmit 'Files restored'
            log "Finished processing action restore files with ids #{p ids} to #{path}"
            op_complete

        # restore dir
        else
            # get all files and folders contained in parent dir
            results = @db.get_details_ids(:dirs, ids)
            dirs = results[:dirs]
            files = results[:files]

            # sanity checks
            if dirs.empty? || files.empty?
                transmit 'No such ids'
                return
            end

            # make directories
            dirs.each do |dir|
                next if dir.nil? || dir.strip.empty?
                dir_path = path + '/' + dir;
                Dir.mkdir dir_path unless Dir.exist? dir_path
            end


            #copy files
            transmit "Copying #{files.length} files to #{path}"
            restore_files(files,path)

            transmit 'Files restored'
            log "Finished processing action restore dir with ids #{p ids} to #{path}"
            op_complete
        end
    end

    # restore files pointed to specified path
    def restore_files(files, path)
        files.each do |file|
            # get backup path
            src = Util::BKP_DIR + '/' + file[:name]
            dest = path + '/' + file[:path]

            # get compression headers
            query = %{ select  chunks
                       from file_manifest
                       where new_name = '#{file[:name]}';
                }

            chunks = @db.get_first_value query
            chunks = chunks.split(';').collect { |x| x.to_i }
            # decompress file
            Util.press_file(src, dest, :decompress, @config[:compress_type], chunks)
        end

    end

    # run query on path
    def query(path, type)
        # query db on ids
        result = @db.get_details_from_path(type, path)

        # no such file?
        # otherwise transmit details
        if result.nil? || result.empty?
            log "#{path} not found "
            transmit "#{path} not found "
            return
        end

        # was the query for files?
        if type == :query_file
            result.each do |tuple|
                mdtime = tuple['mdtime'].to_s
                mdtime = mdtime[0..-6]
                transmit ('Id:%5s Version: %3s Time: %10s Path: %s ' % [tuple['rowid'], tuple['version'], mdtime, tuple['path']])
            end
        # or dirs?
        else
            result.each do |tuple|
                transmit ('Id: %5s Dir : %s ' % [tuple['rowid'], tuple['path']])
            end
        end
        log "Finished processing queries"
    end

    #copy new files to the backup directory
    def copy_new_files
        count = 0

        # files that have been added to db but not copied yet
        file_list =  @db.new_file_list
        transmit 'Please wait while copying files...'

        update_list = []
        file_list.each do |file|
            # populate row with fields of information
            row = {}
            row[:path] = file['path']
            row[:new_name] = Util.rnd_name

            src = row[:path]
            dest = Util::BKP_DIR + row[:new_name]
            fl_size = File.stat(src).size 
            chunks = Util.press_file(src, dest, :compress, @config[:compress_type])
            row[:chunks] = chunks

            # commit to the db
            update_list.push row
            count+= 1
        end

        @db.update_names update_list
        transmit "Copy completed\nCopied #{count} files"
    end

    # transmit to destination pipe
    def transmit str
        Util.puts_pipe(@dest_pipe, str)
    end

    
    # notify change to file monitor
    def notify_monitor
        @notify_m.lock
        @notify_q << 1
        @notify_m.unlock
    end

    # if dir does not exist raise PathError
    def assert_path(path)
        raise PathError unless Dir.exist?(path)
    end

    # if file does not exist raise FileError
    def assert_file(file)
        raise FileError unless File.exist?(file)
    end

    # send the text to the pipe, to show success
    def op_complete
        transmit "Operation completed"
    end

end


main = Daemon.new
main.run
