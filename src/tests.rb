require 'test/unit'

$cmd_line = "ruby src/cmd_line.rb"
$success_string = "Operation complete"

# data and interface tests
class DaemonTests < Test::Unit::TestCase
    # did the operation succeed?
    def op_success? out
        out.include?($success_string)
    end

    # did the operation fail?
    def op_fail? out
        not op_success? out
    end

    # did the query fail?
    def op_query_fail? out
        out.include?("not found")
    end

    # did the query succeed?
    def op_query_success? out
        not op_query_fail? out
    end

    # clear database and files
    def clear_backup
        p "Clearing database"
        output = `#{$cmd_line} --clear`
        op_success? output
    end

    # test for add operation for valid path
    def add_path path
        p "Adding #{path}"
        output = `#{$cmd_line} -a #{path}`
        op_success? output
    end

    # test for file name query
    def query_file_name flname
        p "Querying for file name: #{flname}"
        output = `#{$cmd_line} -q #{flname}`
        op_query_success? output
    end


    # test for dir name query
    def query_dir_name dir_name
        p "Querying for dir name: #{dir_name}"
        output = `#{$cmd_line} -Q #{dir_name}`
        op_query_success? output
    end    

    # test to restore a file
    def restore_file(id, path)
        p "Restoring file with ID #{id} to #{path}"
        output = `#{$cmd_line} -r#{id},#{path}`
        op_success? output
    end

    # test to restore a file
    def restore_dir(id, path)
        p "Restoring dir with ID #{id} to #{path}"
        output = `#{$cmd_line} -R#{id},#{path}`
        op_success? output
    end

    # test to delete a file
    def delete_file(id)
        p "Delete file with ID #{id}"
        output = `#{$cmd_line} -d#{id}`
        op_success? output
    end

    # test to delete a file
    def delete_dir(id)
        p "Delete dir with ID #{id}"
        output = `#{$cmd_line} -D#{id}`
        op_success? output
    end


    def test_valid_data

        path_pic = "/home/dare/pictures"
        # valid tests
        puts ""
        puts "Begining valid tests..."
        assert(clear_backup, "Cannot clear database")

        assert(add_path(path_pic), "Cannot add #{path_pic}")

        assert(query_file_name("troll"), "Querying valid file name : failed")
        assert(query_dir_name("login"), "Querying valid dir name : failed")

        assert(restore_file("1","/home/dare/test"), "Restore a valid file to a valid location : failed")
        assert(restore_dir("3","/home/dare/test"), "Restore a valid dir to a valid location : failed")

        assert(delete_file("1"), "Delete a valid file from the database : failed")
        assert(delete_dir("3"), "Delete a valid dir from the database: failed")

        assert(clear_backup, "Cannot clear database")
    end

    def test_invalid_data

        # invalid tests
        puts ""
        puts "Begining failure tests..."
        path_fail = "/dir_does_not_exists"
        assert(clear_backup, "Cannot clear database")

        assert(!add_path(path_fail), "Added #{path_fail}")

        assert(!query_file_name("file_does_not_exist"), "Querying invalid file name : success")
        assert(!query_dir_name("dir_does_not_exist"), "Querying invalid dir name : success")

        assert(!restore_file("111111111111","/home/dare/test"), "Restore a invalid file to a valid location : success")
        assert(!restore_dir("333333333333","/home/dare/test"), "Restore a invalid dir to a valid location : success")
        assert(!restore_file("not_a_digit","/home/dare/test"), "Restore a invalid file to a valid location : success")
        assert(!restore_dir("not_a_digit","/home/dare/test"), "Restore a invalid dir to a valid location : success")
        assert(!restore_file("2_mix_case","/home/dare/test"), "Restore a invalid file to a valid location : success")
        assert(!restore_dir("mix_case_2","/home/dare/test"), "Restore a invalid dir to a valid location : success")

        assert(!restore_file("1", path_fail), "Restore a valid file to a invalid location : success")
        assert(!restore_dir("3", path_fail), "Restore a valid dir to a invalid location : success")

        assert(!delete_file("111111111111"), "Delete a invalid file from the database : success")
        assert(!delete_dir("333333333333"), "Delete a invalid dir from the database: success")
        assert(!delete_file("not_a_digit"), "Delete a invalid file from the database : success")
        assert(!delete_dir("not_a_digit"), "Delete a invalid dir from the database: success")
        assert(!delete_file("2_mix_case"), "Delete a invalid file from the database : success")
        assert(!delete_dir("mix_case_2"), "Delete a invalid dir from the database: success")

        assert(clear_backup, "Cannot clear database")

    end
end
